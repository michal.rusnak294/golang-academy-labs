#!/bin/bash
#
# script which turns vm into go lang developer's machine (go lang installation)
#
# Checking if distribution is debian-based
#
if which apt-get; then	
  # Adding go lang repositories
  #
  sudo add-apt-repository ppa:longsleep/golang-backports
  sudo apt-get update
  # Declaring version of Go-lang
  #
  GO_VERSION=${GO_VERSION:-1.15}
  echo "Please enter the version of Go you want to install (Press enter to use default value $GO_VERSION):"
  read GO_VERSION
  # Installing Go-lang
  #
  sudo apt-get install golang-go="$GO_VERSION"-go -y
else
  # Checking if distribution is rpm-based
  #  
  if which yum; then
    sudo yum update
    # Declaring version of Go-lang
    #
    GO_VERSION=${GO_VERSION:-1.15}
    echo "Please enter the version of Go you want to install (Press enter to use default value $GO_VERSION):"
    read GO_VERSION
    # Installing Go-lang
    #
    sudo yum install golang-"$GO_VERSION" -y
  else
    echo "This script doesn't support your package manager"
  fi
fi

provider "aws" {
  region = "us-east-1"
  access_key = "$ACCESS_KEY"
  secret_key = "$SECRET_KEY"
}

resource "aws_instance" "ec2-instance-with-k3s" {
  ami           = "ami-0b93ce03dcbcb10f6"
  instance_type = "t2.micro"
  key_name = "key-for-demo"
  vpc_security_group_ids = [aws_security_group.main.id]

  provisioner "remote-exec" {
    inline = [
    "curl -sfL https://get.k3s.io | sh -",
    "sudo k3s kubectl get nodes",
    ]
  }

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file("$PATH_TO_PRIVATE_KEY")
    timeout     = "4m"
   }
}


resource "aws_security_group" "main" {
  egress = [
    {
      cidr_blocks      = [ "0.0.0.0/0", ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    }
  ]
 ingress                = [
   {
     cidr_blocks      = [ "0.0.0.0/0", ]
     description      = ""
     from_port        = 22
     ipv6_cidr_blocks = []
     prefix_list_ids  = []
     protocol         = "tcp"
     security_groups  = []
     self             = false
     to_port          = 22
  }
  ]
}


resource "aws_key_pair" "deployer" {
  key_name = "$PUBLIC_KEY_NAME"
  public_key = "$PUBLIC_KEY"
}


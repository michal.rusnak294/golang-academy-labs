package main

import (
	"fmt"
)

func main() {
	var city string = "Bratislava"
	var state string = "Slovakia"
	fmt.Println("Capital city of", state, "is", city)

}

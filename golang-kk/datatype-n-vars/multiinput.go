package main

import (
	"fmt"
)

func main() {
	var x int
	var y string

	fmt.Printf("Enter integer and string: ")

	count, err := fmt.Scanf("%d %s", &x, &y)

	fmt.Println("Count: ", count)
	fmt.Println("Errors: ", err)
	fmt.Println("Integer: ", x)
	fmt.Println("String: ", y)

}

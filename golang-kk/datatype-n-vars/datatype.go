package main

import "fmt"

func main() {
	// use "backtick" -> ` insted of " incase of printing string
	fmt.Println(`

        string = "abcdefg", "bc348ad", "cloud"

        number = integer = 5456, 1846154, 12
               = float = 5.00, 0.798, 651.54

        boolean = true, false

        array & slices = [1, 5, 11, 7]
                       = ["aaa", "bbb"]
                       = [9.0, 0.99, 11.21]

        maps = "y" -> 55
             =  2 -> 2000
             = "key" -> "value"
`)
}

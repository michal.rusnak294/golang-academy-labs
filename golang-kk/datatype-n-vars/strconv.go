package main

import (
	"fmt"
	"strconv"
)

func main() {
	var i int = 29
	// convert integer to string
	var s string = strconv.Itoa(i)
	fmt.Printf("%q \n", s)
}

package main

import "fmt"

func main() {
	fmt.Println(`
		var s string = "Hello World"
		var i int = 10
		var b bool = false
		var f float64 = 89.12
		`)
}

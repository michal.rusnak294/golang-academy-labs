package main

import (
	"fmt"
)

func main() {
	/* printf - format specifier
		   %v - formats the value in default format
	       %d - formats decimal integers
	*/
	var i int = 81
	var name string = "Michal"
	fmt.Printf("Hello, %v! You have scored %d/100 in Certified Kubernetes Application Developer exam \n", name, i)

	// \n - new line
}

package main

import (
	"fmt"
)

func main() {
	var name string
	var is_rich bool
	fmt.Printf("Enter you name and are you rich (write true if statement is true else write false): ")
	fmt.Scanf("%s %t", &name, &is_rich)
	fmt.Println("Your name is", name, "and statement that you are rich is", is_rich)
}

package main

import (
	"fmt"
)

func main() {
	var (
		s string = "abc"
		i int    = 21
	)
	fmt.Println(s)
	fmt.Println(i)
}

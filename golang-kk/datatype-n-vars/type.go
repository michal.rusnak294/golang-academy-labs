package main

import "fmt"

func main() {
	var number int = 79
	var text string = "Good morning"
	var isGood bool = false
	var amount float64 = 4654.99

	fmt.Printf("Variable number = %v is of type %T \n", number, number)
	fmt.Printf("Variable text = %v is of type %T \n", text, text)
	fmt.Printf("Variable isGood = %v is of type %T \n", isGood, isGood)
	fmt.Printf("Variable amount = %v is of type %T \n", amount, amount)
}

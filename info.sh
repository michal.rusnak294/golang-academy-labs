#!/bin/bash

# script which displays info about server (hostname, distro, IPs, arch, kernel,..)
echo "Your hostname is:"
uname -n
echo "Your Linux Distribution is:"
cat /etc/*-release
echo "Your IPs are:"
ip addr show
echo "Your Linux Architecture is:"
uname -m
echo "Your Linux Kernel is:"
uname -r


#!/bin/bash
#
# script which turns vm into go lang developer's machine (go lang installation)
#
# adding go lang repos
sudo add-apt-repository ppa:longsleep/golang-backports
# updating repos
sudo apt update
#
# Declaring version of go-lang
GO_VERSION=${GO_VERSION:-1.15}
echo "Please enter the version of Go you want to install (Press enter to use default value $GO_VERSION):"
read GO_VERSION
#
# installing go lang
sudo apt install golang-go="$GO_VERSION"-go -y
#

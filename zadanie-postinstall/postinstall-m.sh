#!/bin/bash
# Postinstall for Master Node

# Create User
useradd -s /bin/bash -c "Student" -m student
echo "Passw0rd" | passwd --stdin student
# Set sudo
echo "student ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

###############################################################

## K3S - added
# Install k3s - added
curl -sfL https://get.k3s.io | sh -

# Start the server - added
systemctl start k3s

# Get the join token - added
join_token=$(sudo cat /var/lib/rancher/k3s/server/node-token)

# Set the token as an environment variable - added (this will only works if nodes are on same network...maybe)
echo "export K3S_TOKEN='${join_token}'" >> /etc/environment

# Get the master IP - added
master_ip=$(ip -4 addr show ens3 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')

# Set the master IP as an environment variable - added (this will only works if nodes are on same network...maybe)
echo "export MASTER_IP='${master_ip}'" >> /etc/environment

# Print the join command - added
echo "Run the following command on worker nodes: k3s agent --server https://${master_ip}:6443 --token ${join_token}"

###############################################################

## AWS Route53 stuff
# Variables
ip=`hostname -i`
zoneid=`aws route53 list-hosted-zones-by-name |  jq --arg name "pety.net." -r '.HostedZones | .[] | select(.Name=="\($name)") | .Id' | awk -F / '{ print $1 $3}'`
# Prepare config
cat <<EOF | tee dns.json
{
            "Comment": "CREATE/DELETE/UPSERT a record ",
            "Changes": [{
            "Action": "UPSERT",
                        "ResourceRecordSet": {
                                    "Name": "master.pety.net",
                                    "Type": "A",
                                    "TTL": 300,
                                 "ResourceRecords": [{ "Value": "${ip}"}]
}}]
}
EOF
# Apply
aws route53 change-resource-record-sets --hosted-zone-id ${zoneid} --change-batch file://dns.json
#

## Web server
yum -y install httpd
systemctl enable httpd --now

# CNI - changed to support k3s command
sudo k3s kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
##

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Add aliases - changed k alias to support k3s
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias k=sudo k3s kubectl
alias c=clear
alias s='sudo su -'
alias e=exit
EOF
##

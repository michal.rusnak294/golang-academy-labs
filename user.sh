#!/bin/bash

echo "Creating new directory for work related stuff..."
mkdir /home/work

echo "Adjusting permissons for new directory..."
chmod 755 /home/work

echo "Creating groups..."
groupadd admins
groupadd sap
groupadd db
groupadd engineering

echo "Creating user accounts..."

useradd -m peter -s /bin/bash -g engineering -G admins
useradd -m adam -s /bin/bash -g db -G admins
useradd -m ivan -s /bin/bash -g sap -G admins -u 7331
useradd robot -s /sbin/nologin -g sap -G admins -u 1337

# passwd


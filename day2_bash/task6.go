package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
)

func processFile(file string) {
	fmt.Println(file)
}

func main() {
	files, err := ioutil.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if filepath.Ext(file.Name()) == ".txt" {
			processFile(file.Name())
		}
	}
}

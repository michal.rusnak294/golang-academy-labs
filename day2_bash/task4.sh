# exec >/tmp/debug.$$ 2>$1
set -x # debugger

function abc {
    # this function displays first arg to stdout
    x=$(( $1 + 1))
    echo $x
}

echo "Hello World";
for a in 1 2 3
do
    abc $a
done

# exec >/tmp/debug.$$ 2>$1
set -x # debugger

function process_file {
    # this function displays first arg to stdout
    # args:
    # $1 - file in process
    echo $1
}

for file in `ls -1 *.txt`
# for file in `find . -name \*.txt`
do
    process_file $file
done


#!/bin/bash

echo
echo "Your OS type is:"
uname
echo
echo "Time since last reboot:"
uptime
echo
echo "Memory:"
free -h
echo
echo "File system info:"
df -h | head -2
echo
echo "Network info:"
ip addr | grep -i up
echo
echo "Disk usage:"
du -sh 
echo



